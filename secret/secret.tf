resource "aws_secretsmanager_secret" "secret" {
  name        = "${var.resource_name_prefix}-secretsmanager-${var.name}-01"
  description = var.description
  tags = {
    "name"         = "${var.resource_name_prefix}-secretsmanager-${var.name}-01"
    "resourceType" = "secretsmanager"
    "perimeter"    = "private"
  }
}
