variable "resource_name_prefix" {
  type        = string
  description = "The id of the machine image (AMI) to use for the server."
}

variable "name" {
  type        = string
  description = "The name to use for these cloudwatch resources"
}