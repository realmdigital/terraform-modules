
// Logging and IAM for Site
resource "aws_cloudwatch_log_group" "default" {
  name              = "${var.resource_name_prefix}-lg-${var.name}-01"
  retention_in_days = 30

  tags = {
    "name"         = "${var.resource_name_prefix}-lg-${var.name}-01"
    "resourceType" = "log_group"
    "perimeter"    = "private"
  }
}



// ALB Access Logs
resource "aws_s3_bucket" "alb_access_logs" {
  bucket        = "${var.resource_name_prefix}-s3-alb-access-logs-01"
  force_destroy = true

  tags = {
    "name"         = "${var.resource_name_prefix}-s3-alb-access-logs-01"
    "resourceType" = "s3"
    "perimeter"    = "private"
  }
}

resource "aws_s3_bucket_versioning" "alb_access_logs" {
  bucket = aws_s3_bucket.alb_access_logs.id
  versioning_configuration {
    status = "Suspended"
  }
}

resource "aws_s3_bucket_acl" "alb_access_logs" {
  bucket = aws_s3_bucket.alb_access_logs.id
  acl    = "log-delivery-write"
}

resource "aws_s3_bucket_public_access_block" "alb_access_logs" {
  depends_on = [aws_s3_bucket.alb_access_logs, aws_s3_bucket_policy.alb_access_logs]
  bucket     = aws_s3_bucket.alb_access_logs.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

data "aws_elb_service_account" "main" {}

data "aws_iam_policy_document" "alb_access_logs" {
  policy_id = "s3_bucket_lb_logs"

  statement {
    actions = [
      "s3:PutObject",
    ]
    effect = "Allow"
    resources = [
      "${aws_s3_bucket.alb_access_logs.arn}/*"
    ]

    principals {
      identifiers = [data.aws_elb_service_account.main.arn]
      type        = "AWS"
    }
  }

  statement {
    actions = [
      "s3:PutObject"
    ]
    effect    = "Allow"
    resources = ["${aws_s3_bucket.alb_access_logs.arn}/*"]
    principals {
      identifiers = ["delivery.logs.amazonaws.com"]
      type        = "Service"
    }
  }


  statement {
    actions = [
      "s3:GetBucketAcl"
    ]
    effect    = "Allow"
    resources = [aws_s3_bucket.alb_access_logs.arn]
    principals {
      identifiers = ["delivery.logs.amazonaws.com"]
      type        = "Service"
    }
  }
}

resource "aws_s3_bucket_policy" "alb_access_logs" {
  bucket = aws_s3_bucket.alb_access_logs.id
  policy = data.aws_iam_policy_document.alb_access_logs.json
}

