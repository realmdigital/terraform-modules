# ECS to RDS
resource "aws_security_group" "rds" {
  name        = "${var.resource_name_prefix}-sgrp-rds-01"
  description = "allow inbound access from the tasks only"
  vpc_id      = var.vpc_id

  ingress {
    protocol        = "tcp"
    from_port       = var.rds_instance_details.port
    to_port         = var.rds_instance_details.port
    security_groups = var.ecs_security_group_ids
  }

  egress {
    protocol        = "tcp"
    from_port       = var.rds_instance_details.port
    to_port         = var.rds_instance_details.port
    security_groups = var.ecs_security_group_ids
  }
  tags = {
    "name"         = "${var.resource_name_prefix}-sgrp-rds-01"
    "resourceType" = "security_group"
    "perimeter"    = "private"
  }
}


resource "aws_db_subnet_group" "default" {
  name        = "${var.resource_name_prefix}-rds-subnet-group-01"
  subnet_ids  = var.subnet_ids
  description = "Database subnet group"
  tags = {
    "name"         = "${var.resource_name_prefix}-rds-subnet-group-01"
    "resourceType" = "db_subnet_group"
    "perimeter"    = "private"
  }
}

resource "aws_db_instance" "site" {
  identifier                   = "${var.resource_name_prefix}-rds-site-01"
  db_name                      = var.rds_instance_details.db_name
  username                     = var.rds_instance_admin_credentials.username
  password                     = var.rds_instance_admin_credentials.password
  port                         = var.rds_instance_details.port
  engine                       = var.engine
  engine_version               = var.engine_version
  instance_class               = var.rds_instance_details.instance_size
  allocated_storage            = var.rds_instance_details.allocated_storage
  storage_encrypted            = var.storage_encrypted
  vpc_security_group_ids       = [aws_security_group.rds.id]
  db_subnet_group_name         = aws_db_subnet_group.default.name
  multi_az                     = var.multi_az
  storage_type                 = "gp2"
  publicly_accessible          = false
  allow_major_version_upgrade  = false
  auto_minor_version_upgrade   = true
  apply_immediately            = true
  maintenance_window           = "sun:02:00-sun:04:00"
  skip_final_snapshot          = false
  copy_tags_to_snapshot        = true
  backup_retention_period      = 7
  backup_window                = "04:00-06:00"
  final_snapshot_identifier    = "${var.resource_name_prefix}-rds-site-01-final-${formatdate("YYYYMMDDhhmmss", timestamp())}"
  performance_insights_enabled = false

  lifecycle {
    ignore_changes = [
      password, // https://github.com/hashicorp/terraform-provider-aws/issues/15625
      final_snapshot_identifier,
      engine_version,
    ]
  }

  tags = {
    "name"         = "${var.resource_name_prefix}-rds-site-01"
    "resourceType" = "rds"
    "perimeter"    = "private"
  }
}
