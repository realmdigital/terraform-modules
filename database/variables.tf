variable "resource_name_prefix" {
  type        = string
  description = "The id of the machine image (AMI) to use for the server."
}

variable "multi_az" {
  type        = bool
  description = "Enable Multi-AZ"
}

variable "vpc_id" {
  type        = string
  description = "The VPC Id"
}

variable "engine" {
  type        = string
  description = "Which DB engine"
}

variable "engine_version" {
  type        = string
  description = "Which DB engine version to use"
}

variable "subnet_ids" {
  type        = set(string)
  description = "The subnet IDs"
}

variable "ecs_security_group_ids" {
  type        = set(string)
  description = "The id of the machine image (AMI) to use for the server."
}

variable "rds_instance_details" {
  type = object({
    instance_size     = string
    port              = number
    db_name           = string
    allocated_storage = string
  })
}

variable "rds_instance_admin_credentials" {
  type = object({
    username = string
    password = string
  })
  sensitive = true
}

variable "storage_encrypted" {
  type        = bool
  description = "Enable encryption at rest"
  default     = true
}