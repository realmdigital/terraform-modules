variable "resource_name_prefix" {
  type        = string
  description = "The id of the machine image (AMI) to use for the server."
}

variable "name" {
  type        = string
  description = "Name of service, e.g. site"
}

variable "vpc_id" {
  type        = string
  description = "The VPC Id"
}

variable "cluster_id" {
  type        = string
  description = "ID of the ECS Cluster to add service to"
}

variable "cluster_name" {
  type        = string
  description = "Name of the ECS Cluster to add service to"
}

variable "subnet_ids" {
  type        = list(string)
  description = "The subnet IDs"
}

variable "subnet_cidrs" {
  type        = list(string)
  description = "Subnet CIDR blocks to give full ingress access"
  default     = []
}

variable "ecs_cluster_security_group_id" {
  type        = string
  description = "The security group ID of the ECS Cluster"
}

variable "alb_target_group_id" {
  type        = string
  description = "The ID of the AWS ALB Target Group"
}

variable "alb_security_group_id" {
  type        = string
  description = "The ID of the ALB's Security Group"
}

variable "task_definition_arn" {
  type        = string
  description = "ARN of the ECS Task Definition to attach to this Service"
}

variable "desired_count_min" {
  type        = number
  description = "Minimum numbers of tasks to run"
}

variable "desired_count_max" {
  type        = number
  description = "Maximum numbers of tasks to run"
}

variable "container_name" {
  type        = string
  description = "The name of the container to associate with the load balancer"
}

variable "container_port" {
  type        = number
  description = "Port that container exposes"
  default     = 80
}

variable "assign_public_ip" {
  type        = bool
  description = "Whether to assign public IP address to each task. If using a NAT, this would be set to false, and private subnet ID's used"
  default     = true
}

