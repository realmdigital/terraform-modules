resource "aws_ecs_service" "default" {
  name                    = "${var.resource_name_prefix}-ecs-${var.name}-01"
  cluster                 = var.cluster_id
  task_definition         = var.task_definition_arn
  desired_count           = var.desired_count_min
  launch_type             = "FARGATE"
  platform_version        = "1.4.0"
  propagate_tags          = "TASK_DEFINITION"
  enable_ecs_managed_tags = true

  network_configuration {
    assign_public_ip = var.assign_public_ip
    security_groups  = [var.ecs_cluster_security_group_id, aws_security_group.ecs_service.id]
    subnets          = var.subnet_ids
  }

  load_balancer {
    target_group_arn = var.alb_target_group_id
    container_name   = var.container_name
    container_port   = var.container_port
  }

  lifecycle {
    ignore_changes = [desired_count]
  }

  tags = {
    "name"         = "${var.resource_name_prefix}-ecs-${var.name}-01"
    "resourceType" = "ecs"
    "perimeter"    = "private"
  }
}

# Security Groups
resource "aws_security_group" "ecs_service" {
  name        = "${var.resource_name_prefix}-sgrp-ecs-service-${var.name}-01"
  description = "${var.name} specific security group"
  vpc_id      = var.vpc_id

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    "name"         = "${var.resource_name_prefix}-sgrp-ecs-service-${var.name}-01"
    "resourceType" = "security_group"
    "perimeter"    = "private"
  }
}

resource "aws_security_group_rule" "ecs_alb" {
  security_group_id        = aws_security_group.ecs_service.id
  description              = "${var.name} ALB"
  type                     = "ingress"
  from_port                = var.container_port
  to_port                  = var.container_port
  protocol                 = "tcp"
  source_security_group_id = var.alb_security_group_id
}


resource "aws_security_group_rule" "subnet_cidrs" {
  count             = length(var.subnet_cidrs) > 0 ? 1 : 0
  security_group_id = aws_security_group.ecs_service.id
  type              = "ingress"
  from_port         = 0
  to_port           = 65535
  protocol          = "tcp"
  cidr_blocks       = var.subnet_cidrs
}

# Auto Scaling

resource "aws_appautoscaling_target" "service" {
  min_capacity       = var.desired_count_min
  max_capacity       = var.desired_count_max
  resource_id        = "service/${var.cluster_name}/${aws_ecs_service.default.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_appautoscaling_policy" "service_memory" {
  name               = "${var.resource_name_prefix}-appautoscaling-policy-memory-${var.name}-01"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.service.resource_id
  scalable_dimension = aws_appautoscaling_target.service.scalable_dimension
  service_namespace  = aws_appautoscaling_target.service.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageMemoryUtilization"
    }

    target_value       = 90
    scale_out_cooldown = 60
  }
}

resource "aws_appautoscaling_policy" "service_cpu" {
  name               = "${var.resource_name_prefix}-appautoscaling-policy-cpu-${var.name}-01"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.service.resource_id
  scalable_dimension = aws_appautoscaling_target.service.scalable_dimension
  service_namespace  = aws_appautoscaling_target.service.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }

    target_value       = 90
    scale_out_cooldown = 60
  }
}