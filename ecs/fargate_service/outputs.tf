output "service_name" {
  value = aws_ecs_service.default.name
}

output "service_arn" {
  value = aws_ecs_service.default.id
}

output "security_group_id" {
  value = aws_security_group.ecs_service.id
}