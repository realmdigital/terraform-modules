output "cluster_id" {
  value = aws_ecs_cluster.default.id
}

output "cluster_arn" {
  value = aws_ecs_cluster.default.arn
}

output "cluster_name" {
  value = aws_ecs_cluster.default.name
}

output "security_group_id" {
  value = aws_security_group.ecs.id
}