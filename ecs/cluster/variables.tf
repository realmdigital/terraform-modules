variable "resource_name_prefix" {
  type        = string
  description = "The id of the machine image (AMI) to use for the server."
}

variable "vpc_id" {
  type        = string
  description = "The VPC Id"
}

variable "container_insights" {
  type        = string
  description = "Container Insights"
  default     = "disabled"
}