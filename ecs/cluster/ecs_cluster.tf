// Fargate Cluster
resource "aws_ecs_cluster" "default" {
  name = "${var.resource_name_prefix}-ecs-cls-01"
  setting {
    name  = "containerInsights"
    value = var.container_insights
  }

  tags = {
    "name"         = "${var.resource_name_prefix}-ecs-cls-01"
    "resourceType" = "ecs_cluster"
    "perimeter"    = "private"
  }
}

# ALB TO ECS
resource "aws_security_group" "ecs" {
  name        = "${var.resource_name_prefix}-sgrp-ecs-01"
  description = "allow inbound access from the ALB only"
  vpc_id      = var.vpc_id

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    "name"         = "${var.resource_name_prefix}-sgrp-ecs-01"
    "resourceType" = "security_group"
    "perimeter"    = "private"
  }
}

resource "aws_security_group_rule" "ecs_redis" {
  type              = "ingress"
  description       = "Redis port"
  from_port         = 6379
  to_port           = 6379
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ecs.id
}