resource "aws_lb" "nlb" {
  name               = "${var.resource_name_prefix}-nlb-${var.name}-01"
  internal           = false
  load_balancer_type = "network"

  subnets = var.subnet_ids

  drop_invalid_header_fields = true
  idle_timeout               = 30

  access_logs {
    bucket  = "${var.resource_name_prefix}-s3-alb-access-logs-01"
    prefix  = var.name
    enabled = true
  }
  tags = {
    "name"         = "${var.resource_name_prefix}-nlb-${var.name}-01"
    "resourceType" = "nlb"
    "perimeter"    = "public"
  }
}

resource "aws_lb_target_group" "nlb" {
  name                 = "${var.resource_name_prefix}-lb-tg-${var.name}-01"
  port                 = 2222
  protocol             = "TCP"
  vpc_id               = var.vpc_id
  target_type          = "ip"
  deregistration_delay = 5
  slow_start           = 0

  health_check {
    protocol            = "TCP"
    port                = 2222
    healthy_threshold   = 2
    unhealthy_threshold = 2
    interval            = 10
  }

  tags = {
    "name"         = "${var.resource_name_prefix}-nlb-tg-${var.name}-01"
    "resourceType" = "target_group"
    "perimeter"    = "private"
  }
}

resource "aws_lb_listener" "ssh" {
  depends_on        = [aws_lb.nlb, aws_lb_target_group.nlb]
  load_balancer_arn = aws_lb.nlb.id
  port              = "2222"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.nlb.id
  }
}

# Security Groups

# Internet to NLB
resource "aws_security_group" "nlb" {
  name        = "${var.resource_name_prefix}-sg-nlb-${var.name}-01"
  description = "Allow access on port 2222 only to NLB"
  vpc_id      = var.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    "name"         = "${var.resource_name_prefix}-sg-nlb-${var.name}-01"
    "resourceType" = "security_group"
    "perimeter"    = "private"
  }
}

resource "aws_security_group_rule" "ssh" {
  type              = "ingress"
  from_port         = 2222
  to_port           = 2222
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.nlb.id
}
