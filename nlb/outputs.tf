output "aws_lb_target_group_id" {
  value = aws_lb_target_group.nlb.id
}

output "lb_domain_name" {
  value = aws_lb.nlb.dns_name
}

output "lb_security_group_id" {
  value = aws_security_group.nlb.id
}