data "aws_iam_policy_document" "log_publishing" {
  statement {
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      #"logs:PutLogEventsBatch",
    ]
    resources = ["arn:aws:logs:${var.aws_region}:*:log-group:${var.resource_name_prefix}-lg-${var.name}-01:*"]
  }
}

resource "aws_iam_policy" "log_publishing" {
  name        = "${var.resource_name_prefix}-iam-lg-${var.name}-01"
  path        = "/"
  description = "Allow publishing to cloudwach"

  policy = data.aws_iam_policy_document.log_publishing.json
}

data "aws_iam_policy_document" "secrets_reading" {
  statement {
    actions = [
      "secretsmanager:DescribeSecret",
      "secretsmanager:GetSecretValue",
      "secretsmanager:ListSecrets",
    ]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "secrets_reading" {
  name        = "${var.resource_name_prefix}-iam-secrets-${var.name}-01"
  path        = "/"
  description = "Allow ${var.name} reading from Secrets Manager"

  policy = data.aws_iam_policy_document.secrets_reading.json
}

data "aws_iam_policy_document" "parameters_reading" {
  statement {
    actions = [
      "ssm:GetParameter",
      "ssm:GetParameters",
      "ssm:DescribeParameters"
    ]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "parameters_reading" {
  name        = "${var.resource_name_prefix}-iam-parameters-${var.name}-01"
  path        = "/"
  description = "Allow ${var.name} reading from Parameter Store"

  policy = data.aws_iam_policy_document.parameters_reading.json
}

data "aws_iam_policy_document" "ecr_reading" {
  statement {
    actions = [
      "ecr:BatchCheckLayerAvailability",
      "ecr:BatchGetImage",
      "ecr:GetDownloadUrlForLayer",
      "ecr:GetAuthorizationToken",
    ]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "ecr_reading" {
  name        = "${var.resource_name_prefix}-iam-ecr-${var.name}-01"
  path        = "/"
  description = "Allow -${var.name} reading from ECR"

  policy = data.aws_iam_policy_document.ecr_reading.json
}

data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "role" {
  name               = "${var.resource_name_prefix}-iam-role-${var.name}-01"
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy.json

  tags = {
    "name"         = "${var.resource_name_prefix}-iam-role-${var.name}-01"
    "resourceType" = "iam_role"
    "perimeter"    = "private"
  }
}

resource "aws_iam_role" "task_role" {
  name               = "${var.resource_name_prefix}-iam-task-role-${var.name}-01"
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy.json

  tags = {
    "name"         = "${var.resource_name_prefix}-iam-task-role-${var.name}-01"
    "resourceType" = "iamRole"
    "perimeter"    = "private"
  }
}


resource "aws_iam_role_policy_attachment" "role_log_publishing" {
  role       = aws_iam_role.role.name
  policy_arn = aws_iam_policy.log_publishing.arn
}

resource "aws_iam_role_policy_attachment" "role_secrets_reading" {
  role       = aws_iam_role.role.name
  policy_arn = aws_iam_policy.secrets_reading.arn
}

resource "aws_iam_role_policy_attachment" "role_parameters_reading" {
  role       = aws_iam_role.role.name
  policy_arn = aws_iam_policy.parameters_reading.arn
}

resource "aws_iam_role_policy_attachment" "role_ecr_reading" {
  role       = aws_iam_role.role.name
  policy_arn = aws_iam_policy.ecr_reading.arn
}

