output "role_arn" {
  value = aws_iam_role.role.arn
}

output "role_name" {
  value = aws_iam_role.role.name
}

output "task_role_arn" {
  value = aws_iam_role.task_role.arn
}

output "task_role_name" {
  value = aws_iam_role.task_role.name
}
