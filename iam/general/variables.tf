
variable "resource_name_prefix" {
  type        = string
  description = "Prefix to be used on all created resource names anf tags"
}

variable "aws_region" {
  type        = string
  description = "AWS region to locate of the resources to secure"
}

variable "name" {
  type        = string
  description = "The name to use for these cloudwatch resources"
}
