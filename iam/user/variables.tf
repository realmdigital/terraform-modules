variable "resource_name_prefix" {
  type        = string
  description = "Prefix to be used on all created resource names anf tags"
}

variable "username" {
  type        = string
  description = "Username"
}

variable "path" {
  type        = string
  description = "Path in which to create the user."
  default     = "/"
}

variable "policy_name" {
  type        = string
  description = "IAM Policy name."
}

variable "policy" {
  type        = string
  description = "IAM Policy."
}