resource "aws_iam_user" "default" {
  name = var.username
  path = "/system/"

  tags = {
    "name"         = "${var.resource_name_prefix}-iam-user-01"
    "resourceType" = "iam_user"
    "perimeter"    = "public"
  }
}

# resource "aws_iam_access_key" "default" {
#   user = aws_iam_user.default.name
# }

resource "aws_iam_user_policy" "default" {
  name   = "${var.resource_name_prefix}-iam-policy-${var.policy_name}"
  user   = aws_iam_user.default.name
  policy = var.policy
}
