variable "github_repo" {
  description = "The name of the github repo"
}

variable "github_organisation_name" {
  description = "The name of the organisation that owns the github repo"
}

variable "github_policies" {
  default = []
  type = list(object({
    name   = string
    policy = string
  }))
  description = "A list of policies to create for github."
}

variable "tags" {
  description = "Tags to apply to all resources."
  default     = {}
}
