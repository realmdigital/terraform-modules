resource "aws_iam_openid_connect_provider" "github" {
  client_id_list  = ["sts.amazonaws.com"]
  thumbprint_list = ["6938fd4d98bab03faadb97b34396831e3780aea1"]
  url             = "https://token.actions.githubusercontent.com"
  tags            = var.tags
}

data "aws_iam_policy_document" "github_assume_role_policy" {
  statement {
    principals {
      identifiers = [aws_iam_openid_connect_provider.github.arn]
      type        = "Federated"
    }
    actions = ["sts:AssumeRoleWithWebIdentity"]
    condition {
      test     = "StringEquals"
      variable = "token.actions.githubusercontent.com:aud"
      values   = ["sts.amazonaws.com"]
    }
    condition {
      test     = "StringLike"
      variable = "token.actions.githubusercontent.com:sub"
      values   = ["repo:${var.github_organisation_name}/${var.github_repo}:*"]
    }
  }
}

resource "aws_iam_role" "this" {
  name               = "GithubPipelineRepo"
  assume_role_policy = data.aws_iam_policy_document.github_assume_role_policy.json
  for_each           = var.github_policies
  inline_policy {
    name   = each.value.name
    policy = each.value.policy
  }

  tags = var.tags
}
