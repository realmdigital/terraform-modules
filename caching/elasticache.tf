variable "resource_name_prefix" {}
variable "subnet_id_private_1" {}
variable "subnet_id_private_2" {}
variable "subnet_id_private_3" {}
variable "ecs_security_group_ids" {}

resource "aws_elasticache_subnet_group" "site" {
  name       = "${var.resource_name_prefix}-ec-subnetgroup-site-01"
  subnet_ids = [var.subnet_id_private_1, var.subnet_id_private_2, var.subnet_id_private_3]
}

// ElasicCache
resource "aws_elasticache_cluster" "site" {
  cluster_id           = "${var.resource_name_prefix}-ec-site-01"
  engine               = "redis"
  node_type            = "cache.t3.medium"
  num_cache_nodes      = 1
  parameter_group_name = "default.redis6.x"
  engine_version       = "6.x"
  port                 = 6379
  security_group_ids   = var.ecs_security_group_ids
  subnet_group_name    = "${var.resource_name_prefix}-ec-subnetgroup-site-01"

  lifecycle {
    ignore_changes = [engine_version] // https://github.com/hashicorp/terraform-provider-aws/issues/15625
  }

  tags = {
    "name"         = "${var.resource_name_prefix}-ec-site-01"
    "resourceType" = "elasticache"
    "perimeter"    = "private"
  }

}

output "site_endpoint" {
  value = "${aws_elasticache_cluster.site.cache_nodes.0.address}:${aws_elasticache_cluster.site.cache_nodes.0.port}"
}
