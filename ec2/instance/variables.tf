variable "resource_name_prefix" {
  type        = string
  description = "Resource Name prefix"
}

variable "name" {
  type = string
}

variable "vpc_id" {
  type        = string
  description = "The VPC Id"
}

variable "subnet_id" {
  type        = string
  description = "The subnet ID"
}

variable "security_group_ids" {
  type = list(string)
}

variable "ami" {
  type    = string
  default = "ami-0022f774911c1d690" # Amazon Linux 2 Kernel 5.10 AMI 2.0.20220426.0 x86_64 HVM gp2
}

variable "instance_type" {
  type    = string
  default = "t3a.nano"
}

variable "associate_public_ip_address" {
  type    = bool
  default = false
}

variable "key_pair_name" {
  type    = string
  default = ""
}

variable "user_data" {
  type    = string
  default = ""
}
