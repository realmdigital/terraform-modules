output "public_ip" {
  value = aws_instance.default.public_ip
}

output "public_dns" {
  value = aws_instance.default.public_dns
}
