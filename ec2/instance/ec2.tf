resource "aws_instance" "default" {
  ami                         = var.ami
  instance_type               = var.instance_type
  subnet_id                   = var.subnet_id
  associate_public_ip_address = var.associate_public_ip_address
  key_name                    = length(var.key_pair_name) == 0 ? null : var.key_pair_name
  vpc_security_group_ids      = concat(var.security_group_ids, [aws_security_group.ec2.id])
  user_data                   = length(var.user_data) == 0 ? null : var.user_data
  root_block_device {
    encrypted = true
  }
  lifecycle {
    ignore_changes = [associate_public_ip_address]
  }
  tags = {
    "Name"         = "${var.resource_name_prefix}-ec2-${var.name}-01"
    "name"         = "${var.resource_name_prefix}-ec2-${var.name}-01"
    "resourceType" = "ec2"
    "perimeter"    = "public"
  }
}

resource "aws_security_group" "ec2" {
  name        = "${var.resource_name_prefix}-sgrp-${var.name}-01"
  description = "Allow SSH access"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    description = "Telnet"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow all outbound traffic.
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    "name"         = "${var.resource_name_prefix}-sgrp-ec2-${var.name}-01"
    "resourceType" = "security_group"
    "perimeter"    = "private"
  }
}

resource "aws_ebs_encryption_by_default" "enabled" {
  enabled = true
}
