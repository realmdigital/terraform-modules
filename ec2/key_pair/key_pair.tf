resource "aws_key_pair" "default" {
  key_name   = var.name
  public_key = var.public_key
  tags = {
    "name"         = "${var.resource_name_prefix}-ec2-key-${var.name}-01"
    "resourceType" = "ec2-key"
    "perimeter"    = "private"
  }
}
