variable "resource_name_prefix" {
  type        = string
  description = "Resource Name prefix"
}

variable "name" {
  type = string
}

variable "public_key" {
  type = string
}