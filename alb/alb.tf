resource "aws_alb" "alb" {
  name               = "${var.resource_name_prefix}-alb-${var.name}-01"
  internal           = false
  load_balancer_type = "application"
  enable_http2       = true

  subnets         = var.subnet_ids
  security_groups = [aws_security_group.alb.id]

  drop_invalid_header_fields = true
  idle_timeout               = 30
  access_logs {
    bucket  = "${var.resource_name_prefix}-s3-alb-access-logs-01"
    prefix  = var.name
    enabled = true
  }
  tags = {
    "name"         = "${var.resource_name_prefix}-alb-${var.name}-01"
    "resourceType" = "alb"
    "perimeter"    = "public"
  }
}

resource "aws_alb_target_group" "alb" {
  name                          = "${var.resource_name_prefix}-lb-tg-${var.name}-01"
  port                          = 80
  protocol                      = "HTTP"
  protocol_version              = "HTTP1"
  vpc_id                        = var.vpc_id
  target_type                   = "ip"
  load_balancing_algorithm_type = "least_outstanding_requests"
  deregistration_delay          = 5
  slow_start                    = 0
  health_check {
    path                = var.health_endpoint
    matcher             = "200"
    healthy_threshold   = 2
    unhealthy_threshold = 3
    timeout             = var.health_check_timeout
    interval            = var.health_check_interval
  }

  tags = {
    "name"         = "${var.resource_name_prefix}-lb-tg-${var.name}-01"
    "resourceType" = "target_group"
    "perimeter"    = "private"
  }
}

resource "aws_alb_listener" "http" {
  load_balancer_arn = aws_alb.alb.id
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "fixed-response"
    fixed_response {
      content_type = "text/plain"
      status_code  = "403"
      message_body = "Not Authorised"
    }

  }
}

resource "aws_lb_listener_rule" "http_cloudfront_header_check" {
  listener_arn = aws_alb_listener.http.arn
  priority     = 1

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.alb.id
  }

  condition {
    http_header {
      http_header_name = "x-collation-id"
      values           = ["1235j0345if=340o2fkj359-063ui5534"]
    }
  }
}

resource "aws_alb_listener" "https" {
  count             = length(var.certificate_arn)
  load_balancer_arn = aws_alb.alb.id
  port              = "443"
  protocol          = "HTTPS"
  certificate_arn   = var.certificate_arn

  default_action {
    type = "fixed-response"
    fixed_response {
      content_type = "text/plain"
      status_code  = "403"
      message_body = "Not Authorised"
    }
  }
}

resource "aws_lb_listener_rule" "https_cloudfront_header_check" {
  count        = length(var.certificate_arn)
  priority     = 1
  listener_arn = aws_alb_listener.https[0].arn

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.alb.id
  }

  condition {
    http_header {
      http_header_name = "x-collation-id"
      values           = ["1235j0345if=340o2fkj359-063ui5534"]
    }
  }
}

# Security Groups

# Internet to ALB
resource "aws_security_group" "alb" {
  name        = "${var.resource_name_prefix}-sg-alb-${var.name}-01"
  description = "Allow access on port 443 only to ALB"
  vpc_id      = var.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    "name"         = "${var.resource_name_prefix}-sg-alb-${var.name}-01"
    "resourceType" = "security_group"
    "perimeter"    = "private"
  }
}

resource "aws_security_group_rule" "http" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.alb.id
}

resource "aws_security_group_rule" "https" {
  count             = length(var.certificate_arn)
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.alb.id
}