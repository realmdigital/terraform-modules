variable "resource_name_prefix" {
  type        = string
  description = "The id of the machine image (AMI) to use for the server."
}

variable "name" {
  type        = string
  description = "The name to use for this ALB."
}


variable "vpc_id" {
  type        = string
  description = "The VPC Id"
}

variable "subnet_ids" {
  type        = list(string)
  description = "The subnet IDs"
}

variable "certificate_arn" {
  type        = string
  description = "SSL Certificate ARN"
  default     = ""
}

variable "health_endpoint" {
  type        = string
  description = "Endpoint to run heath check on e.g. /health"
}

variable "health_check_interval" {
  type    = number
  default = 5
}

variable "health_check_timeout" {
  type    = number
  default = 4
}