output "aws_alb_target_group_id" {
  value = aws_alb_target_group.alb.id
}

output "alb_domain_name" {
  value = aws_alb.alb.dns_name
}

output "alb_security_group_id" {
  value = aws_security_group.alb.id
}