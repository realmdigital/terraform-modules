/* Public subnet */
resource "aws_subnet" "public" {
  vpc_id                  = aws_vpc.default.id
  count                   = length(var.public_subnet_cidrs)
  cidr_block              = element(var.public_subnet_cidrs, count.index)
  availability_zone       = element(var.availability_zones, count.index)
  map_public_ip_on_launch = true // makes this a public subnet
  tags = {
    "name"         = "${var.resource_name_prefix}-subnet-public-${element(var.availability_zones, count.index)}"
    "resourceType" = "subnet"
    "perimeter"    = "public"
  }
}

/* Internet gateway allows the internet to access services on the public subnet */
resource "aws_internet_gateway" "default" {
  vpc_id = aws_vpc.default.id
  tags = {
    "name"         = "${var.resource_name_prefix}-igw-default-01"
    "resourceType" = "internet_gateway"
    "perimeter"    = "public"
  }
}

/* Routing table for public subnet */
resource "aws_route_table" "public" {
  vpc_id = aws_vpc.default.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.default.id
  }

  tags = {
    "name"         = "${var.resource_name_prefix}-route-table-public-01"
    "resourceType" = "route_table"
    "perimeter"    = "private"
  }
}

/* Route table associations */
resource "aws_route_table_association" "public" {
  count          = length(var.public_subnet_cidrs)
  subnet_id      = element(aws_subnet.public.*.id, count.index)
  route_table_id = aws_route_table.public.id
}
