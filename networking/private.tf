/* Private subnet */
resource "aws_subnet" "private" {
  vpc_id                  = aws_vpc.default.id
  count                   = length(var.private_subnet_cidrs)
  cidr_block              = element(var.private_subnet_cidrs, count.index)
  availability_zone       = element(var.availability_zones, count.index)
  map_public_ip_on_launch = false // makes this a private subnet
  tags = {
    "name"         = "${var.resource_name_prefix}-subnet-private-${element(var.availability_zones, count.index)}"
    "resourceType" = "subnet"
    "perimeter"    = "private"
  }
}

/* Elastic IP for NAT */
resource "aws_eip" "nat" {
  count      = var.use_nat_gateway == true ? 1 : 0
  vpc        = true
  depends_on = [aws_internet_gateway.default]
  tags = {
    "name"         = "${var.resource_name_prefix}-eip-nat-01"
    "resourceType" = "internet_gateway"
    "perimeter"    = "public"
  }
}

/* NAT Gateway allows services in the Private subnet to access external resources*/
resource "aws_nat_gateway" "default" {
  count         = var.use_nat_gateway == true ? 1 : 0
  allocation_id = aws_eip.nat[0].id
  subnet_id     = element(aws_subnet.public.*.id, 0)
  depends_on    = [aws_internet_gateway.default]
  tags = {
    "name"         = "${var.resource_name_prefix}-nat-default-01"
    "resourceType" = "nat_gateway"
    "perimeter"    = "public"
  }
}

/* Routing table for private subnet */
resource "aws_route_table" "private" {
  vpc_id = aws_vpc.default.id
  count  = var.use_nat_gateway == true ? 1 : 0

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.default[0].id
  }

  tags = {
    "name"         = "${var.resource_name_prefix}-route-table-private-01"
    "resourceType" = "route_table"
    "perimeter"    = "private"
  }
}

resource "aws_route_table_association" "private" {
  count          = var.use_nat_gateway == true ? length(var.private_subnet_cidrs) : 0
  subnet_id      = element(aws_subnet.private.*.id, count.index)
  route_table_id = aws_route_table.private[0].id
}
