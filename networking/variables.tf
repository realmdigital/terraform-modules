variable "resource_name_prefix" {
  type        = string
  description = "Prefix to be used on all created resource names anf tags"
}

variable "aws_region" {
  type        = string
  description = "AWS region to locate VPC"
}

variable "vpc_cidr" {
  type        = string
  description = "CIDR for VPC private IP block"
}

variable "public_subnet_cidrs" {
  type        = list(string)
  description = "Primary public subnet CIDR blocks"
}

variable "availability_zones" {
  type        = list(string)
  description = "Public subnet availability zones"
}

variable "private_subnet_cidrs" {
  type        = list(string)
  description = "Primary private subnet CIDR blocks"
}

variable "use_nat_gateway" {
  type        = bool
  description = "Whether or not to create a NAT Gateway. This is generally not needed, unless a whitelisted IP address is required in a 3rd party service, where there are ever changing IP's of Fargate taks"
  default     = false
}
