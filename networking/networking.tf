resource "aws_vpc" "default" {
  cidr_block                       = var.vpc_cidr
  enable_dns_support               = "true" #gives you an internal domain name
  enable_dns_hostnames             = "true" #gives you an internal host name
  instance_tenancy                 = "default"
  assign_generated_ipv6_cidr_block = false

  tags = {
    "name"         = "${var.resource_name_prefix}-vpc-default-01"
    "resourceType" = "vpc"
    "perimeter"    = "private"
  }
}

/*==== VPC's Default Security Group ======*/
resource "aws_security_group" "default" {
  name        = "${var.resource_name_prefix}-sg-default-01"
  description = "Default security group to allow inbound/outbound from the VPC"
  vpc_id      = aws_vpc.default.id
  depends_on  = [aws_vpc.default]
  # ingress {
  #   from_port = "0"
  #   to_port   = "0"
  #   protocol  = "-1"
  #   self      = true
  # }

  # egress {
  #   from_port = "0"
  #   to_port   = "0"
  #   protocol  = "-1"
  #   self      = "true"
  # }
  tags = {
    "name"         = "${var.resource_name_prefix}-sg-default-01"
    "resourceType" = "security_group"
    "perimeter"    = "private"
  }
}