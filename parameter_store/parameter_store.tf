resource "aws_ssm_parameter" "default" {
  name        = "/${var.resource_name_prefix}/${var.name}-01"
  description = var.description
  type        = var.type
  value       = var.value
  tags = {
    "name"         = "/${var.resource_name_prefix}/${var.name}-01"
    "resourceType" = "parameter"
    "perimeter"    = "private"
  }

  lifecycle {
    ignore_changes = [
      value
    ]
  }
}