variable "resource_name_prefix" {}
variable "name" {}
variable "description" {}
variable "type" {
  type        = string
  description = "The parameter type."
  default     = "SecureString"
}
variable "value" {
  type        = string
  description = "The parameter value."
  default     = "TemporaryValue"
}