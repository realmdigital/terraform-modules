variable "resource_name_prefix" {
  type        = string
  description = "Resource Name prefix"
}

variable "bucket_name" {}

variable "is_static_website" {
  type    = bool
  default = false
}