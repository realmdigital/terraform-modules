output "regional_domain_name" {
  value = aws_s3_bucket.default.bucket_regional_domain_name
}

output "bucket_name" {
  value = aws_s3_bucket.default.bucket
}

output "bucket_arn" {
  value = aws_s3_bucket.default.arn
}

output "bucket_id" {
  value = aws_s3_bucket.default.id
}

output "website_endpoint" {
  value = var.is_static_website == true ? aws_s3_bucket_website_configuration.default[0].website_endpoint : ""
}

output "website_domain" {
  value = var.is_static_website == true ? aws_s3_bucket_website_configuration.default[0].website_domain : ""
}