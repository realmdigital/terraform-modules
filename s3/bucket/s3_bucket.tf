resource "aws_s3_bucket" "default" {
  bucket        = "${var.resource_name_prefix}-${var.bucket_name}"
  force_destroy = true

  tags = {
    "name"         = "${var.resource_name_prefix}-s3-${var.bucket_name}-01"
    "resourceType" = "s3"
    "perimeter"    = "public"
  }
}

resource "aws_s3_bucket_website_configuration" "default" {
  count  = var.is_static_website == true ? 1 : 0
  bucket = aws_s3_bucket.default.bucket

  index_document {
    suffix = "index.html"
  }

}

resource "aws_s3_bucket_versioning" "default" {
  bucket = aws_s3_bucket.default.id
  versioning_configuration {
    status = "Suspended"
  }
}

resource "aws_s3_bucket_acl" "default" {
  bucket = aws_s3_bucket.default.id
  acl    = "private"
}

resource "aws_s3_bucket_public_access_block" "default" {
  bucket = aws_s3_bucket.default.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = var.is_static_website == false
  restrict_public_buckets = var.is_static_website == false
}