resource "aws_cognito_user_pool_client" "default" {
  name                          = var.name
  user_pool_id                  = var.user_pool_id
  enable_token_revocation       = true
  explicit_auth_flows           = ["ALLOW_REFRESH_TOKEN_AUTH", "ALLOW_USER_SRP_AUTH"]
  generate_secret               = false
  prevent_user_existence_errors = "ENABLED"
}