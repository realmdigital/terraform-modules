resource "aws_cognito_user_pool" "default" {
  name = var.name

  admin_create_user_config {
    allow_admin_create_user_only = true
    invite_message_template {
      email_subject = "Realm Digital SSH Key Management - You've been invited"
      email_message = <<EOF
        Visit ${var.sign_in_url} and use the credentials below.<br /><br />
        Your username is {username} and temporary password is {####}
      EOF
      sms_message   = "{username} and {####}"
    }
  }

  auto_verified_attributes = ["email"]

  username_configuration {
    case_sensitive = true
  }

  mfa_configuration = "ON"

  software_token_mfa_configuration {
    enabled = true
  }

  device_configuration {
    challenge_required_on_new_device      = false // this is a confusing setting name, but it Cognito, the set "Trust remembered devices to suppress MFA" to false
    device_only_remembered_on_user_prompt = true
  }

  schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "email"
    required                 = true

    string_attribute_constraints {
      min_length = 0
      max_length = 2048
    }
  }


  account_recovery_setting {
    recovery_mechanism {
      name     = "verified_email"
      priority = 1
    }
  }

  email_configuration {
    email_sending_account = "COGNITO_DEFAULT"
  }

  verification_message_template {
    default_email_option = "CONFIRM_WITH_CODE"
    email_subject        = "Realm Digital SSH Key Management - Your verification code"
    email_message        = "Your verification code is {####}"
  }

}

resource "aws_cognito_user_group" "admin" {
  name         = "Admin"
  user_pool_id = aws_cognito_user_pool.default.id
  precedence   = 1
}