
resource "aws_cloudfront_origin_access_identity" "alb" {}
resource "aws_cloudfront_origin_access_identity" "custom_errors_s3_bucket" {
  comment = var.alternate_domain_names[0]
}

resource "aws_cloudfront_distribution" "site" {
  price_class  = "PriceClass_200"
  enabled      = true
  http_version = "http2and3"
  aliases      = var.alternate_domain_names

  origin {
    domain_name = var.alb_domain_name
    origin_id   = aws_cloudfront_origin_access_identity.alb.id
    custom_origin_config {
      http_port              = 80
      https_port             = 443
      origin_protocol_policy = "http-only"
      origin_ssl_protocols   = ["SSLv3", "TLSv1", "TLSv1.1", "TLSv1.2"]
    }
    custom_header {
      name  = "x-collation-id"
      value = "1235j0345if=340o2fkj359-063ui5534"
    }
  }
  origin {
    domain_name = var.custom_errors_s3_bucket_domain_name
    origin_id   = aws_cloudfront_origin_access_identity.custom_errors_s3_bucket.id

    custom_origin_config {
      origin_protocol_policy = "http-only"
      http_port              = "80"
      https_port             = "443"
      origin_ssl_protocols   = ["SSLv3", "TLSv1", "TLSv1.1", "TLSv1.2"]
    }
  }
  dynamic "custom_error_response" {
    for_each = var.custom_errors_response_code_mappings
    content {
      error_caching_min_ttl = 10
      error_code            = custom_error_response.value
      response_code         = custom_error_response.value
      response_page_path    = "/custom-errors/${custom_error_response.value}"
    }
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  default_cache_behavior {
    allowed_methods  = ["HEAD", "DELETE", "POST", "GET", "OPTIONS", "PUT", "PATCH"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = aws_cloudfront_origin_access_identity.alb.id

    forwarded_values {
      query_string = true

      cookies {
        forward = "all"
      }
      headers = ["*"]
    }

    response_headers_policy_id = aws_cloudfront_response_headers_policy.security_headers_policy.id

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400

  }

  ordered_cache_behavior {
    path_pattern     = "/_nuxt/*"
    cache_policy_id  = "658327ea-f89d-4fab-a63d-7e88639e58f6" // "Managed-CachingOptimized"
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD", "OPTIONS"]
    target_origin_id = aws_cloudfront_origin_access_identity.alb.id

    compress               = true
    viewer_protocol_policy = "redirect-to-https"
  }

  ordered_cache_behavior {
    path_pattern             = "/_next/*"
    cache_policy_id          = "658327ea-f89d-4fab-a63d-7e88639e58f6" // "Managed-CachingOptimized"
    origin_request_policy_id = "216adef6-5c7f-47e4-b989-5492eafa07d3" // "Managed-AllViewer"
    allowed_methods          = ["GET", "HEAD", "OPTIONS"]
    cached_methods           = ["GET", "HEAD", "OPTIONS"]
    target_origin_id         = aws_cloudfront_origin_access_identity.alb.id

    compress               = true
    viewer_protocol_policy = "redirect-to-https"
  }

  ordered_cache_behavior {
    path_pattern     = "/custom-errors/*"
    cache_policy_id  = "658327ea-f89d-4fab-a63d-7e88639e58f6" // "Managed-CachingOptimized"
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = aws_cloudfront_origin_access_identity.custom_errors_s3_bucket.id

    compress               = true
    viewer_protocol_policy = "redirect-to-https"
  }

  viewer_certificate {
    minimum_protocol_version       = length(var.certificate_arn) > 0 ? "TLSv1.2_2021" : "TLSv1"
    acm_certificate_arn            = length(var.certificate_arn) > 0 ? var.certificate_arn : null
    cloudfront_default_certificate = length(var.certificate_arn) == 0
    ssl_support_method             = length(var.certificate_arn) > 0 ? "sni-only" : null
  }

  tags = {
    "name"         = "${var.resource_name_prefix}-cloudfront-site-01"
    "resourceType" = "cloudfront"
    "perimeter"    = "public"
  }
}

data "aws_iam_policy_document" "custom_errors_s3_bucket" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${var.custom_errors_s3_bucket_arn}/*"]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.custom_errors_s3_bucket.iam_arn]
    }
  }
}

resource "aws_s3_bucket_policy" "custom_errors_s3_bucket" {
  bucket = var.custom_errors_s3_bucket_id
  policy = data.aws_iam_policy_document.custom_errors_s3_bucket.json
}

resource "aws_cloudfront_origin_access_control" "s3" {
  name                              = "S3 access control"
  description                       = "S3 access control"
  origin_access_control_origin_type = "s3"
  signing_behavior                  = "always"
  signing_protocol                  = "sigv4"
}

resource "aws_cloudfront_response_headers_policy" "security_headers_policy" {
  name = "default-security-headers-policy"
  security_headers_config {
    content_type_options {
      override = true
    }
    frame_options {
      frame_option = "DENY"
      override     = true
    }
    referrer_policy {
      referrer_policy = "same-origin"
      override        = true
    }
    xss_protection {
      mode_block = true
      protection = true
      override   = true
    }
    strict_transport_security {
      access_control_max_age_sec = "63072000"
      include_subdomains         = true
      preload                    = true
      override                   = true
    }
    content_security_policy {
      content_security_policy = join("; ", compact([
        "frame-ancestors 'none'",
        var.additional_csp.frame_src != "" ? "frame-src ${var.additional_csp.frame_src}" : "frame-src 'none'",
        "default-src 'self'",
        "img-src 'self' data: ${var.additional_csp.img_src}",
        "script-src 'self' ${var.additional_csp.script_src}",
        "style-src 'self' 'unsafe-inline' ${var.additional_csp.style_src}",
        "font-src 'self' ${var.additional_csp.font_src}",
        "object-src 'none'",
        "connect-src 'self' ${var.additional_csp.connect_src}",
        "base-uri 'self'",
        "manifest-src 'self'",
        "form-action 'self' ${var.additional_csp.form_action_src};"
      ]))
      override = true
    }
  }
}
