variable "resource_name_prefix" {
  type        = string
  description = "Resource Name prefix"
}

variable "alb_domain_name" {}
variable "alternate_domain_names" {}

variable "certificate_arn" {
  type        = string
  description = "SSL Certificate ARN"
  default     = ""
}

variable "custom_errors_s3_bucket_domain_name" {}
variable "custom_errors_s3_bucket_arn" {}
variable "custom_errors_s3_bucket_id" {}

variable "custom_errors_response_code_mappings" {
  type    = list(string)
  default = ["503"] //, "404"]
}

variable "additional_csp" {
  type = object({
    connect_src     = optional(string, "")
    font_src        = optional(string, "")
    style_src       = optional(string, "")
    frame_src       = optional(string, "")
    img_src         = optional(string, "")
    form_action_src = optional(string, "")
    script_src      = optional(string, "")
  })
}
